﻿using Azrieli.DAL;
using Azrieli.Models;
using Azrieli.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Azrieli.Controllers
{
    public class GameController : Controller
    {
        //
        // GET: /Game/

        [HttpPost]
        public JsonResult Connect(string facebookID, string email, string fullName)
        {
            string ip = Request.ServerVariables["REMOTE_ADDR"];
            User user = UsersDAL.Create(facebookID, email, fullName, ip);
            bool isSave = user != null;
            return Json(new { result = isSave });
        }

        [HttpPost]
        public JsonResult Play(string facebookID)
        {
            string ip = Request.ServerVariables["REMOTE_ADDR"];
            Game game = GamesDAL.Create(facebookID, ip);

            if (game == null)
            {
                return Json(new
                    {
                        CanPlay = false,
                        Awards = "",
                        Token = ""
                    }
                );
            }

            List<Award> activeAwards = AwardsDAL.ActiveAwards();

            return Json(new 
                {
                    CanPlay = true,
                    Awards = activeAwards.Select(item => item.ID).ToList(),
                    Token = game.Token
                }
            );
        }

        [HttpPost]
        public void Win(Guid gameToken, string facebookID, int awardID, string email, string fullName, string phone)
        {
            GamesDAL.Update(gameToken, facebookID, awardID, email, fullName, phone);
        }

        //[HttpPost]
        public void SendCoupon(string email, int couponID)
        {
            string fileName = couponID + ".png";
            Email emailObj = new Email(HttpContext);
            emailObj.Send("coupon.html", null, email, "Win Machine בקניון עזריאלי - קופון", fileName);
        }
    }
}
