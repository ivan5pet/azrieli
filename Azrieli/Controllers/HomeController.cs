﻿using Azrieli.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Azrieli.DAL;

namespace Azrieli.Controllers
{
//    [RequireHttps]
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Cupon(int id)
        {
            ViewBag.cuponFile = id + ".png";
            return View();
        }
    }
}
