﻿using Azrieli.DAL;
using Azrieli.Models;
using Azrieli.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Configuration;
using System.Web.Security;

namespace Azrieli.Controllers
{
    public class AdminController : Controller
    {
        //
        // GET: /Admin/
        private static string AdminUser
        {
            get
            {
                return ConfigurationManager.AppSettings["AdminUser"];
            }
        }

        private static string AdminPass
        {
            get
            {
                return ConfigurationManager.AppSettings["AdminPass"];
            }
        }
        
        public ActionResult Login()
        {
            if (User.Identity.IsAuthenticated)
                return Redirect("~/Admin");
            return View();
        }

        [HttpPost]
        public ActionResult Login(LoginModel model)
        {
            if (AdminUser != model.UserName || AdminPass != model.Password)
            {
                ModelState.AddModelError("", "הנתונים שהוזנו אינם נכונים");
                return View(model);
            }

            int timeout = model.RememberMe ? 43829 : 60;
            var ticket = new FormsAuthenticationTicket(model.UserName, model.RememberMe, timeout);
            string encrypted = FormsAuthentication.Encrypt(ticket);
            var cookie = new HttpCookie(FormsAuthentication.FormsCookieName, encrypted);
            cookie.Expires = System.DateTime.Now.AddMinutes(timeout);
            Response.Cookies.Add(cookie);

            return Redirect("~/Admin/Users");
        }

        [Authorize(Users = "admin,mangonet")]
        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            Session.Clear();
            return Redirect("~/");
        }

        [Authorize(Users = "admin,mangonet")]
        public ActionResult Users()
        {
            return View();
        }

        [Authorize(Users = "admin,mangonet")]
        public JsonResult UsersGrid()
        {
            KendoGridQueryString qs = new KendoGridQueryString(Request);
            int total = 0;
            List<User> result = UsersDAL.All(out total, qs.FacebookID, qs.CanPalyOnly, qs.Email, qs.FullName,
                qs.SortField, qs.SortDirection, qs.SkipRows, qs.TakeRows);

            return Json(new { data = result, total = total }, JsonRequestBehavior.AllowGet);
        }

        [Authorize(Users = "admin,mangonet")]
        public JsonResult GamesGrid(string facebookID)
        {
            KendoGridQueryString qs = new KendoGridQueryString(Request);
            int total = 0;
            List<Game> result = GamesDAL.All(out total, facebookID, qs.WonOnly, null, null,
                qs.SortField, qs.SortDirection, qs.SkipRows, qs.TakeRows);

            var temp = result.Select(item => new
            {
                item.ID,
                item.UserID,
                item.IP,
                StartDate = item.StartDate.ToEUDateTimeFormat(),
                EndDate = item.EndDate.ToEUDateTimeFormat(),
                AwardName = item.WonAward != null ? item.WonAward.Name : "", //item.WonAward,
                item.Token
            });

            return Json(new { data = temp, total = total }, JsonRequestBehavior.AllowGet);
        }

        [Authorize(Users = "admin,mangonet")]
        public ActionResult Awards()
        {
            return View();
        }

        [Authorize(Users = "admin,mangonet")]
        public JsonResult AwardsGrid()
        {
            KendoGridQueryString qs = new KendoGridQueryString(Request);
            int total = 0;

            List<Award> result = AwardsDAL.All(out total, qs.SortField, qs.SortDirection, qs.SkipRows, qs.TakeRows);

            return Json(new { data = result, total = total }, JsonRequestBehavior.AllowGet);
        }

        [Authorize(Users = "admin,mangonet")]
        [HttpPost]
        public void AwardUpdate(Award award)//int awardID, int amount)
        {
            AwardsDAL.Update(award.ID, award.Amount);
        }
    }
}
