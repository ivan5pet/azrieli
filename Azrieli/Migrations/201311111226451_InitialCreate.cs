namespace Azrieli.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.RASES",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Token = c.Guid(nullable: false),
                        Created = c.DateTime(nullable: false),
                        FullName = c.String(maxLength: 100),
                        FirstName = c.String(maxLength: 40),
                        LastName = c.String(maxLength: 60),
                        Email = c.String(maxLength: 40),
                        Phone = c.String(maxLength: 15),
                        Mobile = c.String(maxLength: 15),
                        Address = c.String(maxLength: 200),
                        City = c.String(maxLength: 20),
                        Street = c.String(maxLength: 40),
                        Building = c.String(maxLength: 8),
                        Apartment = c.Int(nullable: false),
                        Zip = c.String(),
                        IsNewsletter = c.Boolean(),
                        IP = c.String(nullable: false),
                        FacebookID = c.String(nullable: false),
                        RemainingGames = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Games",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        StartDate = c.DateTime(nullable: false),
                        EndDateDate = c.DateTime(),
                        RASESID = c.Int(nullable: false),
                        ProductID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.RASES", t => t.RASESID, cascadeDelete: true)
                .ForeignKey("dbo.Products", t => t.ProductID)
                .Index(t => t.RASESID)
                .Index(t => t.ProductID);
            
            CreateTable(
                "dbo.Products",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Description = c.String(),
                        Amount = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.Games", new[] { "ProductID" });
            DropIndex("dbo.Games", new[] { "RASESID" });
            DropForeignKey("dbo.Games", "ProductID", "dbo.Products");
            DropForeignKey("dbo.Games", "RASESID", "dbo.RASES");
            DropTable("dbo.Products");
            DropTable("dbo.Games");
            DropTable("dbo.RASES");
        }
    }
}
