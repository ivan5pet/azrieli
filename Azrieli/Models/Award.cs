﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Azrieli.Models
{
    public class Award
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [MaxLength(50)]
        public string Name { get; set; }

        [MaxLength(500)]
        public string Description { get; set; }

        [NotMapped]
        public string LogoPath { get; set; }

        [DefaultValue(30)]
        public int Amount { get; set; }

        public ICollection<Game> Games { get; set; }
    }
}