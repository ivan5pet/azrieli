﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Configuration;
using System.Linq;
using System.Web;

namespace Azrieli.Models
{
    public class User
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        public Guid Token { get; set; }

        //[DatabaseGeneratedAttribute(DatabaseGeneratedOption.Computed)]
        //[DefaultValue(typeof(DateTime), DateTime.Now.ToString())]
        public DateTime Created { get; set; }

        [NotMapped]
        public string CreatedStr { get { return Created.ToString(ConfigurationManager.AppSettings["DateFormat"]); } }
        
        [MaxLength(100)]
        public string FullName { get; set; }

        [MaxLength(40)]
        [RegularExpression(@"^[\w-]+(?:\.[\w-]+)*@(?:[\w-]+\.)+[a-zA-Z]{2,7}$", ErrorMessage = "הכתובת אינה תקינה")]
        public string Email { get; set; }

        [MaxLength(15)]
        [RegularExpression(@"^0\d{1,2}(-)?\d{7}$", ErrorMessage = "מס' טלפון לא תקין")]
        public string Phone { get; set; }
        
        [Required]
        [MaxLength(30)]
        public string IP { get; set; }

        [Required]
        [MaxLength(30)]
        public string FacebookID { get; set; }

        [DefaultValue(5)]
        public int RemainingGames { get; set; }
        
        public ICollection<Game> Games { get; set; }

        public User() { }
    }
}