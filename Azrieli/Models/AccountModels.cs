﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Globalization;
using System.Web.Mvc;
using System.Web.Security;

namespace Azrieli.Models
{
    public class LoginModel
    {
        [Required(ErrorMessage = "שם משתמש  הינו שדה חובה")]
        [Display(Name = "שם משתמש: ")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "סיסמא הינו שדה חובה")]
        [DataType(DataType.Password)]
        [Display(Name = "סיסמא: ")]
        public string Password { get; set; }

        [Display(Name = "זכור אותי?")]
        public bool RememberMe { get; set; }

        public static bool CustomValidation(ModelStateDictionary modelState)
        {
            var rememberMe = modelState["RememberMe"].Value.AttemptedValue;
            bool isValid = modelState["Password"].Errors.Count == 0 &&
                modelState["Email"].Errors.Count == 0 &&
                modelState["RememberMe"].Value.AttemptedValue.ToLower() == "on";

            return isValid;
        }
    }
}
