﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Azrieli.Models
{
    public class AzrieliDBContext : DbContext
    {
        public AzrieliDBContext()
            : base("AzrieliDBContext")
        { }

        public DbSet<User> Users { get; set; }
        public DbSet<Award> Awards { get; set; }
        public DbSet<Game> Games { get; set; }
    }
}