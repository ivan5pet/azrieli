var fb = {
	uid:"",
	accessToken:"",
	name:"",
	email:"",
    share: function () {
        var obj = {
            method: 'feed',
            redirect_uri: "https://ssl.publicis-digital.co.il/azrieli/Ynet/",
            link: "https://ssl.publicis-digital.co.il/azrieli/Ynet/",
            picture: "",
            name: "",
            caption: "",
            description: ""
        };
        FB.ui(obj);
        GA.report("Clicks","Share");
    },
    login: function() {
		FB.login(function(response) {
			if (response.authResponse) {
				fb.uid = response.authResponse.userID;
				fb.accessToken = response.authResponse.accessToken;
				FB.api('/me', function(response) {
					fb.name = response.name;
					fb.email = response.email;
					$("object,embed")[0].connected(fb.uid,fb.name,fb.email);
					$.post("Game/Connect",{facebookID:fb.uid, email:fb.email, fullName:fb.name});
			     });
				GA.report("Clicks","Connect");
			} else {
				
			}
		},{scope:"email"});
	}
};

var App = {
	tim:null,
	getUser: function(fbid,accessToken) {
		$.post("../../App/FBConnect",{fbid:fbid,accessToken:accessToken},App.load);
	},
	load:function(res) {
		var normalWidth = 1600;
		var normalHeight = 1000;
		var screenWidth = $(window).width();
		var screenHeight = $(window).height();
		
		var height = screenHeight;
		var ratio = height / normalHeight;
		var width = normalWidth * ratio;
		
		if (width > screenWidth) {
			var width = screenWidth;
			var ratio = width / normalWidth;
			var height = normalHeight * ratio;
		}
		
		var src = "PC.swf?v3";
		if (location.href.indexOf("xxxxxxx") != -1) src = "PC1.swf";

/*
	    $('#Flash').flash(
	        { 
	          src: src,
	          width: width,
	          height: height,
//	          flashvars: { facebookID: res.fbID, remainingPostcards: res.remainingPostcards, accessToken:fb.accessToken }
	        },
	        { version: 10 }
	    );*/
	    $("#Flash").flashembed(src);
	    $("#Flash,object,embed").width(width).height(height);
	    $(window).resize(App.delayResize);
	},
	resize: function() {
		var normalWidth = 1600;
		var normalHeight = 1000;
		var screenWidth = $(window).width();
		var screenHeight = $(window).height();
		
		var height = screenHeight;
		var ratio = height / normalHeight;
		var width = normalWidth * ratio;
		
		if (width > screenWidth) {
			var width = screenWidth;
			var ratio = width / normalWidth;
			var height = normalHeight * ratio;
		}
		
		$("#Flash,object,embed").width(width).height(height);
	},
	delayResize: function() {
		clearTimeout(App.tim);
		App.tim = setTimeout(App.resize,500);
	}
}

var Cupon = {
	print: function(id) {
		$("#cupon").attr("src","Home/Cupon?id="+id);
	},
	send: function(email,id) {
		$.get("Game/SendCoupon",{email:email,couponID:id});
	}
}

var GA = {
	page: function (path) {
        ga('send', 'pageview', path);
    },
    event: function (cat, act, lbl) {
        if (lbl) {
            ga('send', 'event', cat, act, lbl);
        }
        else {
            ga('send', 'event', cat, act);
        }
    },
    report: function (cat, act, lbl) {
    	var path = "/"+cat+"/"+act;
    	if (lbl) path += "/" + lbl;
        GA.page(path);
        GA.event(cat,act,lbl);
    }
}

$(function(){
	App.load();
});