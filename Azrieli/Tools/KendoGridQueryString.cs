﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Azrieli.Tools
{
    public class KendoGridQueryString
    {
        protected HttpRequestBase _context;

        public DAL.Helpers.DALHelpers.SortDirection SortDirection
        {
            get
            {
                string value = _context["sort[0][dir]"] ?? "desc";
                if(value == "desc")
                    return DAL.Helpers.DALHelpers.SortDirection.Desc;

                
                    return DAL.Helpers.DALHelpers.SortDirection.Asc;
            }
        }

        public int SkipRows
        {
            get
            {
                int skip = 0;
                string skipStr = _context["skip"] ?? "";
                int.TryParse(skipStr, out skip);
                return skip;
            }
        }

        public string SortField
        {
            get
            {
                string sortField = _context["sort[0][field]"] ?? "ID";
                switch (sortField)
                {
                    default:
                        return sortField;
                }
            }
        }

        public int TakeRows
        {
            get
            {
                return FormatIntQS(_context["take"] ?? "", 50);
            }
        }

        public string Email
        {
            get
            {
                return FormatStringQS(_context["email"] ?? "");
            }
        }

        public string FacebookID
        {
            get
            {
                return FormatStringQS(_context["facebookID"] ?? "");
            }
        }

        public string FullName
        {
            get
            {
                return FormatStringQS(_context["fullName"] ?? "");
            }
        }

        public bool CanPalyOnly
        {
            get
            {
                return FormatBoolQS(_context["canPalyOnly"]);
            }
        }

        public bool WonOnly
        {
            get
            {
                return FormatBoolQS(_context["wonOnly"]);
            }
        } 

        public KendoGridQueryString(HttpRequestBase context)
        {
            _context = context;
        }

        public string FormatStringQS(string value)
        {
            if (value == "")
                return null;

            value = HttpUtility.HtmlEncode(value.ToLower()).Trim();
            return value;
        }

        public int FormatIntQS(string value, int defaultValue)
        {
            int.TryParse(value, out defaultValue);
            return defaultValue;
        }

        public bool FormatBoolQS(string value)
        {
            if (!string.IsNullOrWhiteSpace(value) && value.ToLower() == "true")
                return true;

            return false;
        }
    }
}