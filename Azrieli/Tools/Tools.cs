﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Azrieli.Tools
{
    public static class Tools
    {
        public static string ToEUDateFormat(this DateTime date)
        {
            return date.ToString("dd/MM/yyyy").Replace('-', '/');
        }

        public static string ToEUDateTimeFormat(this DateTime date)
        {
            return date.ToString("dd/MM/yyyy hh:mm:ss").Replace('-', '/');
        }

        public static string ToEUDateFormat(this DateTime? date)
        {
            if (date == null)
                return null;

            DateTime realDate = (DateTime)date;
            return realDate.ToString("dd/MM/yyyy").Replace('-', '/');
        }

        public static string ToEUDateTimeFormat(this DateTime? date)
        {
            if (date == null)
                return null;

            DateTime realDate = (DateTime)date;
            return realDate.ToString("dd/MM/yyyy hh:mm:ss").Replace('-', '/');
        }
    }
}