﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Web;

namespace Azrieli.Tools
{
    public class Email
    {
        protected HttpContextBase _context;

        protected string SenderEmail
        {
            get
            {
                return ConfigurationManager.AppSettings["EmailFrom"];
            }
        }

        protected string Domain
        {
            get
            {
                return ConfigurationManager.AppSettings["EmailDomain"];
            }
        }

        protected string Port
        {
            get
            {
                return ConfigurationManager.AppSettings["EmailPort"];
            }
        }

        protected string Account
        {
            get
            {
                return ConfigurationManager.AppSettings["EmailAccount"];
            }
        }

        protected string Password
        {
            get
            {
                return ConfigurationManager.AppSettings["EmailPassword"];
            }
        }

        protected string TemplatePath
        {
            get
            {
                return _context.Server.MapPath("~/" + ConfigurationManager.AppSettings["TemplatesFolder"].ToString());
            }
        }

        protected string AttachedFilePath
        {
            get
            {
                return _context.Server.MapPath("~/" + ConfigurationManager.AppSettings["CouponsPath"].ToString());
            }
        }

        public Email(HttpContextBase context)
        {
            _context = context;
        }

        protected string GetTemplate(string templatName)
        {
            string fullName = Path.Combine(TemplatePath, templatName);
            using (StreamReader sr = new StreamReader(fullName))
            {
                string bodyTemplate = sr.ReadToEnd();
                return bodyTemplate;
            }
        }

        protected string FormatBody(string template, Dictionary<string, string> templateParams)
        {
            if (templateParams == null)
                return template;

            foreach (var item in templateParams)
            {
                template = template.Replace(item.Key, item.Value);
            }

            return template;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="templateName"></param>
        /// <param name="templateParams"></param>
        /// <param name="mailTo"></param>
        /// <param name="subject"></param>
        /// <returns></returns>
        public bool Send(string templateName, Dictionary<string, string> templateParams, string mailTo, string subject, string attachedFileName)
        {
            string tempStr = GetTemplate(templateName);
            string body = FormatBody(tempStr, templateParams);
            MailMessage mail = new MailMessage();
            mail.From = new MailAddress(SenderEmail);
            mail.Subject = subject;
            mail.To.Add(mailTo);
            mail.Body = body;
            mail.IsBodyHtml = true;

            string fullFilePath = GetFullFilePath(attachedFileName);
            //FileStream file = null;

            if (fullFilePath != "")
            {
                //file = new FileStream(fullFilePath, FileMode.Open);// .re new Stream(fullFilePath);
                //mail.Attachments.Add(new Attachment(file, "coupon", "image/png"));
                mail.Attachments.Add(new Attachment(fullFilePath));
            }

            SmtpClient SmtpServer = new SmtpClient(Domain);
            SmtpServer.Port = Convert.ToInt32(Port);
            SmtpServer.Credentials = new System.Net.NetworkCredential(Account, Password);
            try
            {
                SmtpServer.Send(mail);
                return true;
            }
            catch
            {
                return false;
            }
            finally
            {
                //file.Close();
                //file.Dispose();
                mail.Dispose();
            }
        }

        protected string GetFullFilePath(string fileName)
        {
            string fullFilePath = "";
            if (fileName == "")
                return "";

            fullFilePath = Path.Combine(AttachedFilePath, fileName);
            if (File.Exists(fullFilePath))
                return fullFilePath;

            return "";
        }
    }
}