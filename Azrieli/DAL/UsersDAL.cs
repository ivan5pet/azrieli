﻿using Azrieli.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web;
using System.Data.Entity;
using Azrieli.DAL.Helpers;

namespace Azrieli.DAL
{
    public static class UsersDAL
    {
        public static User Create(string facebookID, string email, string fullName, string ip)
        {
            using (AzrieliDBContext context = new AzrieliDBContext())
            {
                User user = new User
                {
                    FacebookID = facebookID,
                    Email = email,
                    FullName = fullName,
                    RemainingGames = 10,
                    Created = DateTime.UtcNow,
                    IP = ip,
                    Token = Guid.NewGuid()
                };
                context.Users.Add(user);
                context.SaveChanges();
                return user;
            }
        }

        public static int Create(User user)
        {
            using (AzrieliDBContext context = new AzrieliDBContext())
            {
                context.Users.Add(user);
                context.SaveChanges();
                return user.ID;
            }
        }

        public static User Update(string facebookID, string fullName, string phone, string email)
        {
            using (AzrieliDBContext context = new AzrieliDBContext())
            {
                try
                {
                    User user = context.Users.Single(facebookID);
                    if (user == null)
                        return null;

                    user.FullName = fullName;
                    user.Phone = phone;
                    user.Email = email;

                    context.SaveChanges();
                    return user;
                }
                catch (DbEntityValidationException z)
                {
                    string err = "";
                    foreach (var eve in z.EntityValidationErrors)
                    {
                        err += string.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                            eve.Entry.Entity.GetType().Name, eve.Entry.State);
                        foreach (var ve in eve.ValidationErrors)
                        {
                            err += string.Format("- Property: \"{0}\", Error: \"{1}\"",
                                ve.PropertyName, ve.ErrorMessage);
                        }
                    }
                    throw new Exception(err);
                }
            }
        }

        public static User Single(int userID, IEnumerable<string> IncludedTables)
        {
            using (AzrieliDBContext context = new AzrieliDBContext())
            {
                User user = context.Users.IncludeMulitple(IncludedTables).Single(userID);

                return user;
            }
        }

        public static User Single(string facebookID, IEnumerable<string> IncludedTables)
        {
            using (AzrieliDBContext context = new AzrieliDBContext())
            {
                User user = context.Users.IncludeMulitple(IncludedTables).Single(facebookID);

                return user;
            }
        }

        public static bool CanPlay(string facebookID)
        {
            using (AzrieliDBContext context = new AzrieliDBContext())
            {
                User user = context.Users.Single(facebookID);
                if (user == null)
                    return false;

                return user.RemainingGames > 0;
            }
        }

        public static List<User> All(out int total, string facebookID, bool onlyCanPlay, string email, string fullName,
            string sortField, DALHelpers.SortDirection direction, int skip = 0, int take = 0)
        {
            using (AzrieliDBContext context = new AzrieliDBContext())
            {
                IQueryable<User> query = context.Users.
                    WhereClause(facebookID, onlyCanPlay, email, fullName).
                    SortClause(sortField, direction);

                total = query.Count();

                query = query.SkipTakeClause(skip, take);
                return query.ToList();
            }
        }

        private static IQueryable<User> WhereClause(this IQueryable<User> query, string facebookID, bool onlyCanPlay, string email, string fullName)
        {
            if (facebookID != null)
            {
                query = query.Where(item => item.FacebookID == facebookID);
            }

            if (onlyCanPlay)
            {
                query = query.Where(item => item.RemainingGames > 0);
            }

            if (email != null)
            {
                query = query.Where(item => item.Email.Contains(email));
            }

            if (fullName != null)
            {
                query = query.Where(item =>
                    item.FullName.Contains(fullName)
                );
            }

            return query;
        }

        private static IQueryable<User> SortClause(this IQueryable<User> query, string sortField, DALHelpers.SortDirection direction)
        {
            if (direction == DALHelpers.SortDirection.Desc)
                return query.OrderByDescending(sortField);

            return query.OrderBy(sortField);
        }
    }
}