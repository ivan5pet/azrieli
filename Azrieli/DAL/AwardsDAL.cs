﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Azrieli.DAL.Helpers;
using Azrieli.Models;
using System.Data.Entity;

namespace Azrieli.DAL
{
    public static class AwardsDAL
    {
        public static Award Create(string awardtName, string description, int amount)
        {
            using (AzrieliDBContext context = new AzrieliDBContext())
            {
                Award award = new Award
                {
                    Name = awardtName,
                    Description = description,
                    Amount = amount
                };

                context.Awards.Add(award);
                context.SaveChanges();
                return award;
            }
        }

        public static Award Update(int awardID, int amount)
        {
            using (AzrieliDBContext context = new AzrieliDBContext())
            {
                Award award = context.Awards.Single(awardID);
                if (award == null)
                    return null;

                award.Amount = amount;

                context.SaveChanges();
                return award;
            }
        }

        public static List<Award> ActiveAwards()
        {
            using (AzrieliDBContext context = new AzrieliDBContext())
            {
                return context.Awards.Where(item => item.Amount > 0).ToList();
            }
        }

        public static Award Single(int awardID)
        {
            using (AzrieliDBContext context = new AzrieliDBContext())
            {
                Award award = context.Awards.Single(awardID);
                return award;
            }
        }

        public static List<Award> All(out int total,
            string sortField, DALHelpers.SortDirection direction, int skip = 0, int take = 0)
        {
            using (AzrieliDBContext context = new AzrieliDBContext())
            {
                IQueryable<Award> query = context.Awards.
                    SortClause(sortField, direction);

                total = query.Count();

                query = query.SkipTakeClause(skip, take);
                return query.ToList();
            }
        }

        private static IQueryable<Award> SortClause(this IQueryable<Award> query, string sortField, DALHelpers.SortDirection direction)
        {
            if (direction == DALHelpers.SortDirection.Desc)
                return query.OrderByDescending(sortField);

            return query.OrderBy(sortField);
        }
    }
}