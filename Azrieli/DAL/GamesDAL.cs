﻿using Azrieli.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using Azrieli.DAL.Helpers;

namespace Azrieli.DAL
{
    public static class GamesDAL
    {
        public static Game Create(string facebookID, string ip)
         {
            using (AzrieliDBContext context = new AzrieliDBContext())
            {
                User user = context.Users.Single(facebookID);
                if (user == null || user.RemainingGames < 1)
                    return null;

                Game game = new Game
                {
                    UserID = user.ID,
                    IP = ip,
                    StartDate = DateTime.UtcNow,
                    Token = Guid.NewGuid()
                };
                user.RemainingGames--;

                context.Games.Add(game);
                context.SaveChanges();
                return game;
            }
        }

        public static Game Update(Guid gameToken, string facebookID, int? awardID, string email, string fullName, string phone)
        {
            using (AzrieliDBContext context = new AzrieliDBContext())
            {
                DateTime updatingTime = DateTime.UtcNow;
                Game game = context.Games.Single(gameToken, facebookID);
                if (game == null)
                    return null;

                if (game.EndDate != null)
                    return null;

                if (updatingTime.AddMinutes(-5) > game.StartDate)
                    return null;


                game.EndDate = DateTime.UtcNow;
                game.AwardID = awardID;

                game.User.Email = email;
                game.User.FullName = fullName;
                game.User.Phone = phone;

                if (awardID != null)
                {
                    Award product = context.Awards.Single((int)awardID);
                    product.Amount--;
                }

                context.SaveChanges();
                return game;
            }
        }

        public static Game LatestByUserID(int userID)
        {
            using (AzrieliDBContext context = new AzrieliDBContext())
            {
                Game game = context.Games.
                    Last(userID); 
                return game;
            }
        }

        public static Game Single(string facebookID)
        {
            using (AzrieliDBContext context = new AzrieliDBContext())
            {
                Game game = context.Games.
                    Last(facebookID);
                return game;
            }
        }

        public static List<Game> All(out int total, string facebookID, bool onlyWin, string email, string fullName,
            string sortField, DALHelpers.SortDirection direction, int skip = 0, int take = 0)
        {
            using (AzrieliDBContext context = new AzrieliDBContext())
            {
                IQueryable<Game> query = context.Games.
                    Include("WonAward").
                    WhereClause(facebookID, onlyWin, email, fullName).
                    SortClause(sortField, direction);

                total = query.Count();

                query = query.SkipTakeClause(skip, take);
                return query.ToList();
            }
        }

        private static IQueryable<Game> WhereClause(this IQueryable<Game> query, string facebookID, bool onlyWin, string email, string fullName)
        {
            if (facebookID != null)
            {
                query = query.Where(item => item.User.FacebookID == facebookID);
            }

            if (onlyWin)
            {
                query = query.Where(item => item.AwardID != null);
            }

            if (email != null)
            {
                query = query.Where(item => item.User.Email.Contains(email));
            }

            if (fullName != null)
            {
                query = query.Where(item =>
                    item.User.FullName.Contains(fullName)
                );
            }

            return query;
        }

        private static IQueryable<Game> SortClause(this IQueryable<Game> query, string sortField, DALHelpers.SortDirection direction)
        {
            if (direction == DALHelpers.SortDirection.Desc)
                return query.OrderByDescending(sortField);

            return query.OrderBy(sortField);
        }
    }
}